// if the database is empty on server start, create some sample data.
Meteor.startup(function () {
  if (Plants.find().count() === 0) {
    var data = [
      {
        name: "Benjamin",
        species: "Ficus benjamina"
      }
    ];

    var timestamp = (new Date()).getTime();
    _.each(data, function(list) {
      var plant_id = Plants.insert(list);
    });
  }
});
