Meteor.publish('plants', function() {
  return Plants.find({});
});

Meteor.publish("userData", function () {
  if (this.userId) {
    return Meteor.users.find({_id: this.userId}, { fields: {
      'username': 1,
      'emails': 1,
      'createdAt': 1,
      'profile': 1
    }});
  } else {
    this.ready();
  }
});
