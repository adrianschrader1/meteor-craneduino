Template.plantsView.helpers({
  isEditable: function() {
    return Session.get('editable');
  },
  afterRemoval: function() {

  }
});

Template.plantsView.events({
  "click .updatePlantEdit": function(event, template){
    event.preventDefault();
    Session.set('editable', true);
  },
  "click .updatePlantSave": function(event, template){
    if (AutoForm.validateForm('updatePlantForm'))
      Session.set('editable', false);
  },
  "click .deletePlant": function(event, template){
    Session.set('editable', false);
  }
});

AutoForm.hooks({
  deletePlant: {
    after: {
      remove: function(err) {

        Router.go('/plants/' + Plants.findOne()._id);
      }
    }
  }
});
