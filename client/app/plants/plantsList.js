var currentObj;

Template.plantsList.helpers({
  plants: function() {
    currentObj = this;
    return Plants.find({});
  }
});

Template.plantsList.events({
  "submit .insertPlant": function(event) {
    event.preventDefault();

    Meteor.call("insertPlant", {
      name: event.target.name.value
    });

    event.target.name.value = "";
  }
});

Template.plant.helpers({
  isActive: function(){
    if (!currentObj) {
      return false;
    }
    return (currentObj._id == this._id);
  }
});

Template.plant.events({
  "click .list-group-item": function(event, template){
    event.preventDefault();
    if (!Session.get('editable')) {
      Session.set('editable', false);
      Router.go('/plants/' + template.data._id);
    }
  }
});
