Meteor.subscribe("userData");

Template.loginView.onRendered(function() {
  if (Meteor.userId()) {
    Router.go('/');
  }
});
