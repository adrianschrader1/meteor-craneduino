Template.loginCtrl.onCreated(function() {
  this.errorMessage = new ReactiveVar('');
});

Template.loginCtrl.helpers({
  errorMessage: function() {
    return Template.instance().errorMessage.get();
  },
  showErrorMessage: function() {
    return Template.instance().errorMessage.get() !== '';
  }
});

Template.loginCtrl.events({
  "submit .form-signin": function(event, template) {
    event.preventDefault();

    if (!event.target.username.value || !event.target.password.value) {
      return;
    }

    Meteor.loginWithPassword(event.target.username.value, event.target.password.value, function(err) {
      if (err) {
        template.errorMessage.set(err);
        event.target.username.value = '';
        event.target.password.value = '';
      }
      else {
        Router.go('/');
      }
    });
  }
});
