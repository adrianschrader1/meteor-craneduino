Template.topbar.helpers({
  isLoggedIn: function () {
    return Meteor.userId();
  },
  routes: function() {
    var routes = [{
      name: 'dashboard',
      displayName: 'Dashboard',
      path: '/'
    },
    {
      name: 'plantsView',
      displayName: 'Pflanzen',
      path: '/plants'
    }];

    _.each(routes, function(route) {
      route.active = (route.name == Router.current().route.getName());
    });

    return routes;
  },
  isProfilePage: function() {
    return 'profileView' == Router.current().route.getName();
  }
});

Template.topbar.events({
  "click .btn-login": function(event, template){
    event.preventDefault();
    Router.go('/accounts/login');
  },
  "click .btn-signup": function(event, template){
    event.preventDefault();
    Router.go('/accounts/signup');
  },
  "click .logout": function(event, template) {
    Accounts.logout();
  }
});
