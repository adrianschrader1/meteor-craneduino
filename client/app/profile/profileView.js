
Template.profileView.helpers({
});

Template.profileView.events({
  "submit .profileForm": function(event, template){
    event.preventDefault();

    var profile = {
      firstName: event.target.firstName.value,
      lastName: event.target.lastName.value,
      bio: event.target.bio.value,
      website: event.target.website.value
    };

    if (Meteor.users.simpleSchema().namedContext().validateOne({ profile: profile }, "profile")) {
      Meteor.call("updateUserProfile", profile);
    } else {
      
    }
  }
});
