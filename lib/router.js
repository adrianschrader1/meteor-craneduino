// Default routing settings
Router.configure({
  layoutTemplate: 'index',
  notFoundTemplate: '404'
});

// Redirect to the login page
Router.onBeforeAction(function() {
  if (! Meteor.userId()) {
    Router.go('/accounts/login');
    this.next();
  } else {
    this.next();
  }
});

// Routes
routes = {
  'dashboard':  {
    name: 'Dashboard',
    description: 'overview & stats',
    path: [ '/', '/dashboard']
  },
  'plantsView': {
    name: 'Plants',
    description: 'details about registered plants',
    path: '/plants/:_id',
    waitOn: function() {
      return Meteor.subscribe('plants');
    },
    data: function() {
      return Plants.findOne(this.params._id);
    }
  },
  'loginView': {
    name: 'Login',
    description: 'login page',
    path: '/accounts/:action',
    data: function() {
      return { isSignup: this.params.action == 'signup' };
    }
  },
  'profileView': {
    name: 'Profile',
    description: 'update user information',
    path: '/accounts',
    data: function() {
      return Meteor.user();
    }
  }
};

// Redirects
Router.route('/plants', {
  waitOn: function() {
    Meteor.subscribe('plants');
  },
  action: function() {
    this.redirect('/plants/' + Plants.findOne({ })._id);
  }
});

Router.map(function() {
  _.each(routes, function(route, url) {
    Router.route(url, { path: route.path, data: route.data, waitOn: route.waitOn });
  });
});
