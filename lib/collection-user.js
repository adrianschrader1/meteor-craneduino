var Schema = {};

var UserProfile = new SimpleSchema({
  firstName: {
    label: "Vorname",
    type: String,
    optional: true
  },
  lastName: {
    label: "Nachname",
    type: String,
    optional: true
  },
  bio: {
    label: "Biographie",
    type: String,
    optional: true
  },
  website: {
    label: "Website",
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true
  }
});

Schema.User = new SimpleSchema({
  username: {
    type: String,
    optional: true,
  },
  emails: {
    label: 'Email-Adressen',
    type: Array,
    optional: true
  },
  "emails.$": {
    type: Object,
    optional: true
  },
  "emails.$.address": {
    label: 'Adresse',
    type: String,
    regEx: SimpleSchema.RegEx.Email
  },
  "emails.$.verified": {
    label: 'Verifiziert',
    type: Boolean
  },
  createdAt: {
    type: Date,
    denyUpdate: true
  },
  profile: {
    label: 'Profilinformationen',
    type: UserProfile,
    optional: true
  },
  // Make sure this services field is in your schema if you're using any of the accounts packages
  services: {
    type: Object,
    optional: true,
    blackbox: true
  },
  roles: {
    type: Object,
    optional: true,
    blackbox: true
  },
  // In order to avoid an 'Exception in setInterval callback' from Meteor
  heartbeat: {
    type: Date,
    optional: true
  }
});

Meteor.users.attachSchema(Schema.User);

Security.defineMethod("ifIsCurrentUser", {
  fetch: [],
  transform: null,
  deny: function (type, arg, userId, doc) {
    console.log(doc);
    return userId !== doc._id;
  }
});

Meteor.users.permit( [ 'update', 'remove' ]).apply();
/**
Meteor.users.permit( [ 'update', 'remove' ]).ifLoggedIn().ifIsCurrentUser().exceptProps([
  'username', 'createdAt', 'services', 'roles', 'heartbeat', 'emails.$.verified'
]).apply();
Meteor.users.permit( [ 'insert', 'update', 'remove' ] ).ifLoggedIn().ifHasRole('admin').exceptProps([
  'services', 'heartbeat', 'createdAt', 'emails.$.verified'
]).apply();
*/
