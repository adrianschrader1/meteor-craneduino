Meteor.methods({
  "insertPlant": function(plant) {
    plant.createdOn = new Date();
    Plants.insert(plant);
  },
  "updateUserProfile": function(profile) {
    console.log(profile);
    Meteor.users.update(Meteor.userId, { $set: { profile: profile } });
  }
});
