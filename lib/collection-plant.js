Plants = new Mongo.Collection('plants');

var plantSchema = new SimpleSchema({
  name: {
    type: String,
    label: "Name",
    min: 3,
    max: 50
  },
  species: {
    type: String,
    label: "Art",
    max: 100,
    optional: true
  },
  notes: {
    type: String,
    label: "Anmerkungen",
    optional: true,
    autoform: {
      afFieldInput: {
        type: "textarea"
      }
    }
  },
  wateringTime: {
    type: Number,
    label: "Bewässerungszeit",
    min: 1000,
    max: 20000,
    autoValue: function() {
      return 5000;
    }
  },
  moisture: {
    type: Number,
    label: "Feuchtigkeit (0-1023)",
    min: 0,
    max: 1023,
    autoValue: function() {
      return 500;
    }
  },
  createdAt: {
    type: Date,
    label: "Erstellt am",
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  },
  updatedAt: {
    type: Date,
    label: "Aktualisiert am",
    autoValue: function() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true
  },
  createdBy: {
    type: String,
    label: "Erstellt von",
    autoValue: function() {
      if (this.isInsert) {
        return Meteor.userId();
      } else if (this.isUpsert) {
        return {$setOnInsert: Meteor.userId()};
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  }
});

Plants.attachSchema(plantSchema);

Plants.permit(['insert', 'update', 'remove']).ifLoggedIn().apply();
